function xy -a f -d "Open path in desktop application"
    if test -n "$f"
        xdg-open "$f"
    else
        xdg-open "$PWD"
    end

end
