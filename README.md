xy - xy is not y
----------------
open files and directories in desktop applications quickly

---

xy is a small handy function to open files and directories quickly in their corresponding desktop applications by utilizing `xdg-open`.

# installation

using fisher:
```sh
fisher add gitlab.com/Scrumplex/xy
```

# usage
```
# open current directory in desktop file manager
$ xy

# open some file in text editor
$ xy readme.txt

# open some directory in file manager
$ xy docs
```
you can also use `xx` instead of `xy` as this may be easier on qwerty keyboards.

# license
licensed under bla bla **GPL3**
